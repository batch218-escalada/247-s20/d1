console.log('Hello World');

// [SECTION] While Loop

    // A While Loop takes in an expression/condition. 

    let count = 5;

    while (count !== 0) {
        console.log('While: ' + count);

        count--;
    };

    let grade = 1;

    while (grade <= 5) {
        console.log('I am grade ' + grade + ' student');
        // Without the second statement, this will create an infinite loop.

        grade++;
        // Thus, putting a second statement is necessary in preventing an infinite loop

    };

    /* 
        Syntax:
            while (condition/expression) {
                statement;
            };
    */

    // Another example

    let a = 1;
    let b = 5;

    while (a <= b) {
        console.log('Apple ' + a);

        a++;
    };

// [SECTION] Do While Loop

    // A Do While Loop works a lot like the while loop. But unlike While Loops, do-while loops guarantee that the code will be executed at least once.

    /*
        Syntax:
            do {
            
            } while (condition/expression);
    */

    let number = Number(prompt('Give me a number'));
    console.log(number);
        console.log(typeof number);

        do {

            // The current value of the number is printed out.
            console.log('Do While: ' + number);

            // Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater.
            // number = number +1.
            number += 1;
        } while (number < 10);

    // Another example

    let c = 1;
    let d = 5;

    do {
        console.log('Carrots ' + c);
        c++;
    } while (c <= d);

// [SECTION] For Loop

    // A For Loop is more flexible than While and Do-While loops. It consists of three parts:
        // 1. The 'initialization' value that will track the progression of the loop.
        // 2. The 'expression/condition' that will be evaluatedwhich will determine whether the loop will run one more time.
        // 3. The 'finalExpression' indicates how to advance the loop.

        let f = 5;

        for (let e = 0; e <= f; ++e) {
            console.log('Eggplant ' + e);
        };

        /* 
            Syntax: 
                for(Initialization; expression/condition; finalExpression){
                    statement;
                };
        */

        for (let count = 0; count <= 10; count++) {
            console.log(count);
        };

        let myStrings = 'Albert';
        console.log(myStrings);
        console.log(myStrings.length);

            console.log(myStrings[0]);
            console.log(myStrings[1]);
            console.log(myStrings[2]);
        
        for (let x = 0; x < myStrings.length; x++) {
            console.log(myStrings[x]);
        };

    // Create a string named 'myName' with a value of your name.

        let myName = 'Albert';

        for (let i = 0; i < myName.length; i++) {
            if (
                myName[i].toLowerCase() == 'a' ||
                myName[i].toLowerCase() == 'e' ||
                myName[i].toLowerCase() == 'i' ||
                myName[i].toLowerCase() == 'o' ||
                myName[i].toLowerCase() == 'u'
            ) {
                console.log(3);
            } else {
                console.log(myName[i]);
            };
        };

// [SECTION] Continue and Break Statements

    // The Continue statment allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
    // The break statments is used to terminate the current loop once a match has been found.

    // Simple Example of Break

    for (let g = 1; g <= 12; g++) {
        if (g === 6) {
            break;
        console.log('Grapes ' + g);
        };
    };

    // Simple Example of Continue

    for (let i = 1; i <= 12; i++) {
        if (i === 6) {
            continue;

        console.log('Indian Mango ' + i);
        };  
    };

    for (let count = 0; count <= 20; count++) {
        if (count % 2 === 0) {
            continue;
        }
        console.log('Continue and Break: ' + count);

        if (count > 12) {
            break;

        // Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less that or equal to 20.

        };
    };

    let name = 'Alejandro';

    for (let i = 0; i < name.length; i++) {
        console.log(name[i]);

        if (name[i].toLowerCase() === 'a') {
            console.log('Continue to the next iteration');
            continue;
        }
        
        // If the current letter is equal to d, stop the loop.
        if (name[i] == 'd') {
            break;
        };
    };